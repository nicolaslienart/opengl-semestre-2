#include "draw.h"

void primitive_addVertex(Primitive *primitive, VertexPosition position, VertexColour colour) {

	VertexElement *newVertex = malloc(sizeof(VertexElement));
	if (!newVertex) return EXIT_FAILURE;

	newVertex->position = position;
	newVertex->colour = colour;
	newVertex->next = primitive->firstVertex;
	primitive->firstVertex = newVertex;
}

Primitive *primitive_addPrimitive(Primitive *last, GLenum type) {
	Primitive *newPrimitive = malloc(sizeof(Primitive));
	if(!newPrimitive) return EXIT_FAILURE;

	newPrimitive->type = type;
	newPrimitive->firstVertex = NULL;
	newPrimitive->next = last;

	return newPrimitive;
}

void vertex_setPosition(VertexPosition *vertex, int x, int y, int windowWidth, int windowHeight) {
	vertex->x = -1 + 2. * x / windowWidth;
	vertex->y = -(-1 + 2. * y / windowHeight);
}

void vertex_drawVertex(VertexElement *vertex){
	glColor3ub(vertex->colour.r, vertex->colour.g, vertex->colour.b);
	glVertex2f(vertex->position.x, vertex->position.y);
}

void primitive_drawPrimitive(Primitive *primitive) {

	VertexElement *vertex = primitive->firstVertex;

	if (vertex) {
		glBegin(primitive->type);

		while (vertex) {
			vertex_drawVertex(vertex);
			vertex = vertex->next;
		}

		glEnd();
	}
}

void artboard_drawAll(Primitive *firstPrimitive){
	Primitive *primitive = firstPrimitive;

	while (primitive) {
		primitive_drawPrimitive(primitive);
		primitive = primitive->next;
	}
}

void artboard_drawColorWheel(int totalAngles) {

	if (totalAngles > 360) {
		totalAngles = 360;
	}

	double angleStep = 2 * M_PI / totalAngles;
	double currentAngle = 0;
	float currentAngleDeg;

	glBegin(GL_TRIANGLE_FAN);
	glColor3ub(255,255,255);
	VertexColour currentColour;
	glVertex2f(0,0);

	while (currentAngle < 2 * M_PI) {
		currentAngleDeg = currentAngle * (180 / M_PI);
		colour_HSVtoRGB(&currentColour, currentAngleDeg, 1, 1);	
		glColor3ub(currentColour.r, currentColour.g, currentColour.b);
		glVertex2f(0.8 * cos(currentAngle), 0.8 * sin(currentAngle) + 0.1);
		currentAngle += angleStep;
	}
	glEnd();

	glBegin(GL_QUADS);
	glColor3ub(0,0,0);
	glVertex2f(1,-0.8);
	glVertex2f(1,-1);
	glColor3ub(255,255,255);
	glVertex2f(-1,-1);
	glVertex2f(-1,-0.8);
	glEnd();
}

void artboard_drawOriginScaled() {
	glPushMatrix();
	artboard_scaleToOrtho2D();
	artboard_drawOrigin();
	glPopMatrix();
}

void artboard_drawOrigin() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_LINES);
	glColor3ub(255,0,0);
	glVertex2f(-1, 0);
	glVertex2f(1, 0);
	glColor3ub(0,255,0);
	glVertex2f(0, 1);
	glVertex2f(0, -1);
	glEnd();
}

void artboard_drawSquare(int full) {

	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_QUADS);
	glVertex2f(-0.5, 0.5);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.5, -0.5);
	glVertex2f(-0.5, -0.5);
	glEnd();
}

void artboard_drawCircle(int full) {
	float angleStep = 2 * M_PI / 360;
	float angleCurrent = 0;
	
	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_POLYGON);

	while(angleCurrent < 2 * M_PI) {
		glVertex2f(0.5 * cos(angleCurrent), 0.5 * sin(angleCurrent));
		angleCurrent += angleStep;
	}

	glEnd();
}

float cursor_getAngleDeg(float x, float y) {
	float angle;
	float module = sqrt((x * x) + (y * y));
	angle = acos(x / module);
	angle *= 180;
	printf("%lf°\n", angle);
	return angle;
}

void artboard_drawRoundedSquare(int full) {
	float temp;
	VertexPosition vectorArc = {0.4, 0.4};

	glPolygonMode(GL_FRONT_AND_BACK, full);

	float angleStep = 2 * M_PI / 360;
	float angleCurrent = 0;
	
	glBegin(GL_POLYGON);

	for (int i = 1; i <= 4; i++) {

		while(angleCurrent < ((M_PI / 2) * i)) {
			glVertex2f(0.1 * sin(angleCurrent) + vectorArc.x, 0.1 * cos(angleCurrent) + vectorArc.y);
			angleCurrent += angleStep;
		}

		if (vectorArc.x == vectorArc.y) {
			vectorArc.y = -vectorArc.y;
		}
		else {
			if (vectorArc.x > 0) {
				vectorArc.x = -vectorArc.x;
			}
			else {
				vectorArc.y = -vectorArc.y;
			}
		}
	}

	glEnd();
}

void artboard_scaleToOrtho2D() {
	GLfloat projectionMatrix[16];
	GLfloat x_axis, y_axis; 
	glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrix);

	x_axis = pow(projectionMatrix[0], -1);
	y_axis = pow(projectionMatrix[5], -1);

	glScalef(x_axis, y_axis, 0);
}

void arm_drawFirst() {
	glPushMatrix();

	glScalef(2, 2, 0);
	artboard_drawCircle(GL_FILL);
	glPopMatrix();	
	glPushMatrix();
	glTranslatef(0, 8, 0);
	artboard_drawCircle(GL_FILL);

	glPopMatrix();	

	glBegin(GL_POLYGON);
	glVertex2f(1, 0);
	glVertex2f(-1, 0);
	glVertex2f(-0.5, 8);
	glVertex2f(0.5, 8);
	glEnd();
}

void arm_drawSecond() {
	glPushMatrix();

	artboard_drawRoundedSquare(GL_LINE);

	glPushMatrix();

	glTranslatef(0, 8, 0);
	artboard_drawRoundedSquare(GL_LINE);

	glPopMatrix();	

	glTranslatef(0, 4, 0);
	glScalef(0.5, 8, 0);
	artboard_drawSquare(GL_FILL);

	glPopMatrix();	
}

void arm_drawThird() {
	glPushMatrix();

	glPushMatrix();

	glTranslatef(0, 4, 0);
	glScalef(0.5, 8, 0);
	artboard_drawSquare(GL_FILL);

	glPopMatrix();

	glTranslatef(0, 8, 0);
	artboard_drawCircle(GL_LINE);

	glPopMatrix();	
}

GLuint create_firstArm() {
	GLuint id = glGenLists(1);
	glNewList(id, GL_COMPILE);
	arm_drawFirst();
	glEndList();
	return id;
}

GLuint create_secondArm() {
	GLuint id = glGenLists(1);
	glNewList(id, GL_COMPILE);
	arm_drawSecond();
	glEndList();
	return id;
}

GLuint create_thirdArm() {
	GLuint id = glGenLists(1);
	glNewList(id, GL_COMPILE);
	arm_drawThird();
	glEndList();
	return id;
}

void updateAlpha(float *alpha, int *direction) {
	if (*alpha >= 45) {
		*direction = DRAW_CLOCKWISE;
	}
	else if (*alpha <= -45) {
		*direction = DRAW_ANTI_CLOCKWISE;
	}
	if (*direction == DRAW_CLOCKWISE) {
		*alpha -= 1;
	}
	else {
		*alpha += 1;
	}
}

void artboard_drawAllArms(float alpha, float beta, float gamma, GLuint arms[]) {
	glPushMatrix();

	glRotatef(45, 0, 0, 1);

	glRotatef(alpha, 0, 0, 1);
	glCallList(arms[0]);
	glTranslatef(0, 8, 0);

	glRotatef(beta, 0, 0, 1);
	glCallList(arms[1]);
	glTranslatef(0, 8, 0);

	glRotatef(-gamma, 0, 0, 1);
	glCallList(arms[2]);
	glRotatef(gamma, 0, 0, 1);
	glCallList(arms[2]);
	glRotatef(gamma, 0, 0, 1);
	glCallList(arms[2]);

	glPopMatrix();
}

void clock_drawBackground() {
	glColor3ub(200,200,200);
	artboard_drawCircle(GL_FILL);
	glColor3ub(0,0,0);
	glPushMatrix();
	glScalef(0.98, 0.98, 1);
	artboard_drawCircle(GL_LINE);
	glLineWidth(3);
	glPopMatrix();
}

void clock_drawSteps() {
	int fullRotation = 360;
	int angleStep = fullRotation / (5 * 12);
	int angleCurrent = 0;
	glPushMatrix();
	glRotatef(90, 0, 0, 1);
	glScalef(0.8, 0.8, 1);
	float angleRad;
	float scale[2];

	while(angleCurrent < fullRotation) {
		scale[0] = 5;
		scale[1] = 1;
		angleRad = angleCurrent * M_PI / (fullRotation / 2);

		glColor3ub(255, 0, 0);
		
		if ((angleCurrent % (fullRotation / 12)) == 0) {
			glColor3ub(0, 0, 0);
			scale[0] *= 1.7;
			scale[1] *= 1.5;
		}

		glPushMatrix();

		glTranslatef(0.5 * cos(angleRad), 0.5 * sin(angleRad), 1);
		glScalef(0.01, 0.01, 1);
		glRotatef(angleCurrent, 0, 0, 1);
		glScalef(scale[0], scale[1], 1);
		artboard_drawSquare(GL_FILL);

		glPopMatrix();

		angleCurrent += angleStep;
	}
	glPopMatrix();

}

void clock_drawHand(float rotation, float weight, float lenght) {
	float handThickness = 3 * weight;
	float handLenght = 60 * lenght;

	glColor3ub(255, 0, 0);

	glPushMatrix();

	glScalef(0.01, 0.01, 1);
	glRotatef(-rotation * 6, 0, 0, 1);
	glTranslatef(0, handLenght / 100 * 30, 0);
	glScalef(handThickness, handLenght, 1);
	artboard_drawSquare(GL_FILL);

	glPopMatrix();
}

void artboard_drawClock(int hour, int min, int sec) {
	float hourTime = (hour % 12) ? hour % 12 : hour;
	hourTime = hourTime * 60 / 12 + min / 12;
	float minTime = min + sec / (12 * 5.);
	clock_drawBackground();
	glColor3ub(255, 0, 0);
	clock_drawSteps();
	clock_drawHand(hourTime, 0.8, 0.5);
	clock_drawHand(minTime, 0.5, 0.7); 
	clock_drawHand(sec, 0.2, 0.8);
}

void primitive_drawDigit(GLuint textureID) {
	//glColor3ub(0,0,255);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);
	glVertex2f(-.5,.5);
	glTexCoord2f(1,0);
	glVertex2f(.5,.5);
	glTexCoord2f(1,1);
	glVertex2f(.5,-.5);
	glTexCoord2f(0,1);
	glVertex2f(-.5,-.5);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}
