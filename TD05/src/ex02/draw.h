#ifndef DRAW_H_
#define DRAW_H_

#include <GL/gl.h>
#include <stdlib.h>
#include <stdio.h>

#include "colour.h"

enum rotationWay {
	DRAW_ANTI_CLOCKWISE,
	DRAW_CLOCKWISE
};

typedef struct VertexPosition {
	float x;
	float y;
} VertexPosition;

typedef struct VertexElement {
	VertexColour colour;
	VertexPosition position;
	struct VertexElement *next;
} VertexElement;

typedef struct Primitive {
	GLenum type;
	VertexElement *firstVertex;
	struct Primitive *next;
} Primitive;

void primitive_addVertex(Primitive *primitive, VertexPosition position, VertexColour colour);

Primitive *primitive_addPrimitive(Primitive *last, GLenum type);

void vertex_setPosition(VertexPosition *vertex, int x, int y, int windowWidth, int windowHeight);

void vertex_drawVertex(VertexElement *vertex);

void primitive_drawPrimitive(Primitive *primitive);

void artboard_drawAll(Primitive *firstPrimitive);

void artboard_drawOriginScaled();

void artboard_drawOrigin(); 

void artboard_drawSquare(int full);

void artboard_scaleToOrtho2D();

void artboard_drawCircle(int full);

float cursor_getAngleDeg(float x, float y);

void artboard_drawRoundedSquare(int full);

/** @brief Print a colour picker wheel
 *
 * @param totalAngles Defines the total precision of the wheel
 */
void artboard_drawColorWheel(int totalAngles);

void printMatrix();

void arm_drawFirst();

void arm_drawSecond();

void arm_drawThird();

GLuint create_firstArm();
GLuint create_secondArm();
GLuint create_thirdArm();

void updateAlpha(float *alpha, int *direction);
void artboard_drawAllArms(float alpha, float beta, float gamma, GLuint arms[]);

void clock_drawBackground();
void clock_drawHand(float rotation, float weight, float lenght);
void artboard_drawClock(int hour, int min, int sec);

void primitive_drawDigit(GLuint textureID);
#endif // DRAW_H_
