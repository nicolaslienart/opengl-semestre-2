#include "textures.h"

void digits_loadToSDL(SDL_Surface *digits[], int size) {
	char path[] = "src/ex02/numbers/";
	char digit[2];
	char ext[] = ".png";
	char fullPath[50];

	for (int i = 0; i < size; i++) {
		fullPath[0] = '\0';
		sprintf(digit, "%d", i);
		digit[1] = '\0';
		strcat(fullPath, path);
		strcat(fullPath, digit);
		strcat(fullPath, ext);

		printf("path : %s\n", fullPath);

		digits[i] = IMG_Load(fullPath);

		if (!digits[i]) {
			fprintf(stderr, "Unable to load digit %d\n", i);
			return EXIT_FAILURE;
		}
	}
}

void digits_loadToGL(SDL_Surface *digits[], int size, GLuint textures[]) {

	glGenTextures(size, textures);


	for (int i = 0; i < size; i++) {
		glBindTexture(GL_TEXTURE_2D, textures[i]);
		GLint check;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &check);
		if(check == GL_INVALID_OPERATION) {
		}
		printf("%d %d \n", digits[i]->w, digits[i]->h);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, digits[i]->w, digits[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, digits[i]->pixels);
		if (glGetError() == GL_INVALID_ENUM) {
			printf("Invalid Texture target");
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
