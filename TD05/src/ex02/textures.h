#ifndef TEXTURE_H_ 
#define TEXTURE_H_ 

#include <string.h>
#include <GL/gl.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

void digits_loadToSDL(SDL_Surface *digits[], int size);

void digits_loadToGL(SDL_Surface *digits[], int size, GLuint textures[]);

#endif // TEXTURE_H_
