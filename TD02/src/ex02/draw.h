#ifndef DRAW_H_
#define DRAW_H_

#include <GL/gl.h>
#include <stdlib.h>
#include <stdio.h>

#include "colour.h"

typedef struct VertexPosition {
	float x;
	float y;
} VertexPosition;

typedef struct VertexElement {
	VertexColour colour;
	VertexPosition position;
	struct VertexElement *next;
} VertexElement;

typedef struct Primitive {
	GLenum type;
	VertexElement *firstVertex;
	struct Primitive *next;
} Primitive;

void primitive_addVertex(Primitive *primitive, VertexPosition position, VertexColour colour);

Primitive *primitive_addPrimitive(Primitive *last, GLenum type);

void vertex_setPosition(VertexPosition *vertex, int x, int y, int windowWidth, int windowHeight);

void vertex_drawVertex(VertexElement *vertex);

void primitive_drawPrimitive(Primitive *primitive);

void artboard_drawAll(Primitive *firstPrimitive);

void artboard_drawOrigin(int full); 

void artboard_drawSquare(int full);

void artboard_drawCircle(int full);


/** @brief Print a colour picker wheel
 *
 * @param totalAngles Defines the total precision of the wheel
 */
void artboard_drawColorWheel(int totalAngles);
#endif // DRAW_H_
