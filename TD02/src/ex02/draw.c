#include "draw.h"

void primitive_addVertex(Primitive *primitive, VertexPosition position, VertexColour colour) {

	VertexElement *newVertex = malloc(sizeof(VertexElement));
	if (!newVertex) return EXIT_FAILURE;

	newVertex->position = position;
	newVertex->colour = colour;
	newVertex->next = primitive->firstVertex;
	primitive->firstVertex = newVertex;
}

Primitive *primitive_addPrimitive(Primitive *last, GLenum type) {
	Primitive *newPrimitive = malloc(sizeof(Primitive));
	if(!newPrimitive) return EXIT_FAILURE;

	newPrimitive->type = type;
	newPrimitive->firstVertex = NULL;
	newPrimitive->next = last;

	return newPrimitive;
}

void vertex_setPosition(VertexPosition *vertex, int x, int y, int windowWidth, int windowHeight) {
	vertex->x = -1 + 2. * x / windowWidth;
	vertex->y = -(-1 + 2. * y / windowHeight);
}

void vertex_drawVertex(VertexElement *vertex){
	glColor3ub(vertex->colour.r, vertex->colour.g, vertex->colour.b);
	glVertex2f(vertex->position.x, vertex->position.y);
}

void primitive_drawPrimitive(Primitive *primitive) {

	VertexElement *vertex = primitive->firstVertex;

	if (vertex) {
		glBegin(primitive->type);

		while (vertex) {
			vertex_drawVertex(vertex);
			vertex = vertex->next;
		}

		glEnd();
	}
}

void artboard_drawAll(Primitive *firstPrimitive){
	Primitive *primitive = firstPrimitive;

	while (primitive) {
		primitive_drawPrimitive(primitive);
		primitive = primitive->next;
	}
}

void artboard_drawColorWheel(int totalAngles) {

	if (totalAngles > 360) {
		totalAngles = 360;
	}

	double angleStep = 2 * M_PI / totalAngles;
	double currentAngle = 0;
	float currentAngleDeg;

	glBegin(GL_TRIANGLE_FAN);
	glColor3ub(255,255,255);
	VertexColour currentColour;
	glVertex2f(0,0);

	while (currentAngle < 2 * M_PI) {
		currentAngleDeg = currentAngle * (180 / M_PI);
		colour_HSVtoRGB(&currentColour, currentAngleDeg, 1, 1);	
		glColor3ub(currentColour.r, currentColour.g, currentColour.b);
		glVertex2f(0.8 * cos(currentAngle), 0.8 * sin(currentAngle) + 0.1);
		currentAngle += angleStep;
	}
	glEnd();

	glBegin(GL_QUADS);
	glColor3ub(0,0,0);
	glVertex2f(1,-0.8);
	glVertex2f(1,-1);
	glColor3ub(255,255,255);
	glVertex2f(-1,-1);
	glVertex2f(-1,-0.8);
	glEnd();
}

void artboard_drawOrigin(int full) {

	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_LINES);
	glColor3ub(255,0,0);
	glVertex2f(-1, 0);
	glVertex2f(1, 0);
	glColor3ub(0,255,0);
	glVertex2f(0, 1);
	glVertex2f(0, -1);
	glEnd();
}

void artboard_drawSquare(int full) {

	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_QUADS);
	glColor3ub(0,0,255);
	glVertex2f(-0.5, 0.5);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.5, -0.5);
	glVertex2f(-0.5, -0.5);
	glEnd();
}

void artboard_drawCircle(int full) {
	float angleStep = 2 * M_PI / 360;
	float angleCurrent = 0;
	
	glPolygonMode(GL_FRONT_AND_BACK, full);

	glColor3ub(0,0,155);

	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0,0); // Center of the circle

	while(angleCurrent < 2 * M_PI) {
		glVertex2f(0.5 * cos(angleCurrent), 0.5 * sin(angleCurrent));
		angleCurrent += angleStep;
	}

	glEnd();
}
