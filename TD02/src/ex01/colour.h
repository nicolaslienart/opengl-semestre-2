#ifndef COLOUR_H_
#define COLOUR_H_

#include <GL/gl.h>
#include <math.h>

typedef struct VertexColour {
	GLubyte r;
	GLubyte g;
	GLubyte b;
} VertexColour;

/* @brief Convert HSV entry to RGB
 * 
 * @param rgb The colour structure to be filled as the result
 * @param h,s,v Hue, saturation and value (luminosity)
 *
 * @param[out] translated RGB value is put in pointer rgb
 */
void colour_HSVtoRGB(VertexColour *rgb, float hue, float saturation, float value);

#endif // COLOUR_H_
