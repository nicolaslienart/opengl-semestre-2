#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>

#include "colour.h"
#include "draw.h"

/* Dimensions et titre de la fenetre */
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "TD02 Minimal";

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


void reshape(SDL_Surface** surface) {
	SDL_Surface* surface_temp = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, SDL_OPENGL | SDL_RESIZABLE | SDL_GL_DOUBLEBUFFER);
	if(NULL == surface_temp) {
		fprintf(
				stderr, 
				"Erreur lors du redimensionnement de la fenetre.\n");
		return;
	}
	*surface = surface_temp;

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1., 1., -1., 1.);
}


int main(int argc, char** argv) 
{
	/* Initialisation de la SDL */
	if(-1 == SDL_Init(SDL_INIT_VIDEO)) 
	{
		fprintf(
				stderr, 
				"Impossible d'initialiser la SDL. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	SDL_Surface* surface;
	reshape(&surface);
	//surface = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, SDL_OPENGL | SDL_GL_DOUBLEBUFFER);
	if(NULL == surface) 
	{
		fprintf(
				stderr, 
				"Impossible d'ouvrir la fenetre. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Initialisation du titre de la fenetre */
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);

	VertexPosition currentPostion;
	VertexColour currentColour;

	currentColour.r = 255;
	currentColour.g = 255;
	currentColour.b = 255;

	GLboolean moveYellowSquare = GL_FALSE; 
	GLboolean rotateYellowSquare = GL_FALSE; 

	double yellowSquarePosition[2] = {0,0};
	float yellowSquareRotation = 0;


	float xStartMove = 0.;
	float yStartMove = 0.;
	float xEndMove = 0.;
	float yEndMove = 0.;

	float startAngle = 0.;
	float endAngle = 0.;

	int sens = 1;

	/* Boucle principale */
	int loop = 1;
	while(loop) 
	{
		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();

		/* Placer ici le code de dessin */
		glClear(GL_COLOR_BUFFER_BIT);
		gluOrtho2D(-4, 4, -3, 3);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		artboard_drawOrigin();
		glTranslatef(1, 2, 0);
		glColor3ub(255,155,155);
		artboard_drawCircle(GL_FILL);

		glTranslatef(-1, -2, 0);
		glColor3ub(25,178,45);
		glRotatef(45, 0, 0, 1);
		artboard_drawSquare(GL_FILL);

		glColor3ub(215,178,45);
		glRotatef(45, 0, 0, 1);
		glTranslatef(-1, -2, 0);
		artboard_drawSquare(GL_FILL);

		glLoadIdentity();
		
		glTranslatef(yellowSquarePosition[0], yellowSquarePosition[1], 0);
		glRotatef(yellowSquareRotation, 0, 0, 1);
		glColor3ub(0,0,255);
		artboard_drawSquare(GL_FILL);


		/* Echange du front et du back buffer : mise a jour de la fenetre */
		SDL_GL_SwapBuffers();

		/* Boucle traitant les evenements */
		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}

			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}

			if(	e.type == SDL_KEYDOWN 
					&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
			{
				loop = 0; 
				break;
			}

			/* Quelques exemples de traitement d'evenements : */
			switch(e.type) 
			{
				/* Redimensionnement fenetre */
				case SDL_VIDEORESIZE:
					WINDOW_WIDTH = e.resize.w;
					WINDOW_HEIGHT = e.resize.h;
					reshape(&surface);
					break;

					/* Clic souris */
				case SDL_MOUSEBUTTONDOWN:

					xEndMove = (float) 8. * e.button.x / WINDOW_WIDTH - 4;
					yEndMove = (float) -(6. * e.button.y / WINDOW_HEIGHT) + 3;
					
					endAngle = cursor_getAngleDeg(xEndMove, yEndMove);

					if (e.button.button == SDL_BUTTON_LEFT) {

						moveYellowSquare = GL_TRUE;

					}
					else if (e.button.button == SDL_BUTTON_RIGHT) {
						//yellowSquareRotation = cursor_getAngleDeg(xEndMove, yEndMove);
						rotateYellowSquare = GL_TRUE;

					}

					printf("clic en (%d, %d)\n", e.button.x, e.button.y);
					break;

				case SDL_MOUSEBUTTONUP:
					moveYellowSquare = rotateYellowSquare = GL_FALSE;
					break;

				case SDL_MOUSEMOTION:

					xStartMove = xEndMove;
					yStartMove = yEndMove;

					xEndMove = 8. * e.motion.x / WINDOW_WIDTH - 4;
					yEndMove = -(6. * e.motion.y / WINDOW_HEIGHT) + 3;

					startAngle = endAngle;
					endAngle = cursor_getAngleDeg(xEndMove, yEndMove);

					if (rotateYellowSquare) {
						/*
						if (startAngle - endAngle < 0 && sens == 1) {
							yellowSquareRotation -= fabs(startAngle - endAngle);	
						}
						else if (startAngle - endAngle < 0) {
							yellowSquareRotation -= fabs(startAngle - endAngle);	
						}
						else if (startAngle - endAngle >= 0 && sens == 1) {
							yellowSquareRotation -= fabs(startAngle - endAngle);	
						}
						else {
							yellowSquareRotation += fabs(startAngle - endAngle);	
						}
						*/
						if (startAngle - endAngle >= 0) {
							yellowSquareRotation -= startAngle - endAngle;
						}
						else if (startAngle - endAngle < 0) {
							yellowSquareRotation -= fabs(startAngle - endAngle);	
						}
						printf("starta %f enda %f\n", startAngle, endAngle);
					}

					if (moveYellowSquare) {

						printf("startx %f endx %f\n", xStartMove, xEndMove);
						printf("starty %f endy %f\n\n", yStartMove, yEndMove);

						yellowSquarePosition[0] += xEndMove - xStartMove;
						yellowSquarePosition[1] += yEndMove - yStartMove;
					}

					break;

					/* Touche clavier */
				case SDL_KEYDOWN:
					printf("touche pressee (code = %d)\n", e.key.keysym.sym);
					break;

				default:
					break;
			}
		}

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;
		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
	}

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();

	return EXIT_SUCCESS;
}
