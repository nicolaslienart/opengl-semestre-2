#include "draw.h"

void primitive_addVertex(Primitive *primitive, VertexPosition position, VertexColour colour) {

	VertexElement *newVertex = malloc(sizeof(VertexElement));
	if (!newVertex) return EXIT_FAILURE;

	newVertex->position = position;
	newVertex->colour = colour;
	newVertex->next = primitive->firstVertex;
	primitive->firstVertex = newVertex;
}

Primitive *primitive_addPrimitive(Primitive *last, GLenum type) {
	Primitive *newPrimitive = malloc(sizeof(Primitive));
	if(!newPrimitive) return EXIT_FAILURE;

	newPrimitive->type = type;
	newPrimitive->firstVertex = NULL;
	newPrimitive->next = last;

	return newPrimitive;
}

void vertex_setPosition(VertexPosition *vertex, int x, int y, int windowWidth, int windowHeight) {
	vertex->x = -1 + 2. * x / windowWidth;
	vertex->y = -(-1 + 2. * y / windowHeight);
}

void vertex_drawVertex(VertexElement *vertex){
	glColor3ub(vertex->colour.r, vertex->colour.g, vertex->colour.b);
	glVertex2f(vertex->position.x, vertex->position.y);
}

void primitive_drawPrimitive(Primitive *primitive) {

	VertexElement *vertex = primitive->firstVertex;

	if (vertex) {
		glBegin(primitive->type);

		while (vertex) {
			vertex_drawVertex(vertex);
			vertex = vertex->next;
		}

		glEnd();
	}
}

void artboard_drawAll(Primitive *firstPrimitive){
	Primitive *primitive = firstPrimitive;

	while (primitive) {
		primitive_drawPrimitive(primitive);
		primitive = primitive->next;
	}
}

void artboard_drawColorWheel(int totalAngles) {

	if (totalAngles > 360) {
		totalAngles = 360;
	}

	double angleStep = 2 * M_PI / totalAngles;
	double currentAngle = 0;
	float currentAngleDeg;

	glBegin(GL_TRIANGLE_FAN);
	glColor3ub(255,255,255);
	VertexColour currentColour;
	glVertex2f(0,0);

	while (currentAngle < 2 * M_PI) {
		currentAngleDeg = currentAngle * (180 / M_PI);
		colour_HSVtoRGB(&currentColour, currentAngleDeg, 1, 1);	
		glColor3ub(currentColour.r, currentColour.g, currentColour.b);
		glVertex2f(0.8 * cos(currentAngle), 0.8 * sin(currentAngle) + 0.1);
		currentAngle += angleStep;
	}
	glEnd();

	glBegin(GL_QUADS);
	glColor3ub(0,0,0);
	glVertex2f(1,-0.8);
	glVertex2f(1,-1);
	glColor3ub(255,255,255);
	glVertex2f(-1,-1);
	glVertex2f(-1,-0.8);
	glEnd();
}

void artboard_drawOriginScaled() {
	glPushMatrix();
	artboard_scaleToOrtho2D();
	artboard_drawOrigin();
	glPopMatrix();
}

void artboard_drawOrigin() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_LINES);
	glColor3ub(255,0,0);
	glVertex2f(-1, 0);
	glVertex2f(1, 0);
	glColor3ub(0,255,0);
	glVertex2f(0, 1);
	glVertex2f(0, -1);
	glEnd();
}

void artboard_drawSquare(int full) {

	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_QUADS);
	glVertex2f(-0.5, 0.5);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.5, -0.5);
	glVertex2f(-0.5, -0.5);
	glEnd();
}

void artboard_drawCircle(int full) {
	float angleStep = 2 * M_PI / 360;
	float angleCurrent = 0;
	
	glPolygonMode(GL_FRONT_AND_BACK, full);

	glBegin(GL_POLYGON);

	while(angleCurrent < 2 * M_PI) {
		glVertex2f(0.5 * cos(angleCurrent), 0.5 * sin(angleCurrent));
		angleCurrent += angleStep;
	}

	glEnd();
}

float cursor_getAngleDeg(float x, float y) {
	float angle;
	float module = sqrt((x * x) + (y * y));
	angle = acos(x / module);
	angle *= 180;
	printf("%lf°\n", angle);
	return angle;
}

void artboard_drawRoundedSquare(int full) {
	float temp;
	VertexPosition vectorArc = {0.4, 0.4};

	glPolygonMode(GL_FRONT_AND_BACK, full);

	float angleStep = 2 * M_PI / 360;
	float angleCurrent = 0;
	
	glBegin(GL_POLYGON);

	for (int i = 1; i <= 4; i++) {

		while(angleCurrent < ((M_PI / 2) * i)) {
			glVertex2f(0.1 * sin(angleCurrent) + vectorArc.x, 0.1 * cos(angleCurrent) + vectorArc.y);
			angleCurrent += angleStep;
		}

		if (vectorArc.x == vectorArc.y) {
			vectorArc.y = -vectorArc.y;
		}
		else {
			if (vectorArc.x > 0) {
				vectorArc.x = -vectorArc.x;
			}
			else {
				vectorArc.y = -vectorArc.y;
			}
		}
	}

	glEnd();
}

void artboard_scaleToOrtho2D() {
	GLfloat projectionMatrix[16];
	GLfloat x_axis, y_axis; 
	glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrix);

	x_axis = pow(projectionMatrix[0], -1);
	y_axis = pow(projectionMatrix[5], -1);

	glScalef(x_axis, y_axis, 0);
}

void arm_drawFirst() {
	glPushMatrix();

	glScalef(2, 2, 0);
	artboard_drawCircle(GL_FILL);
	glPopMatrix();	
	glPushMatrix();
	glTranslatef(0, 8, 0);
	artboard_drawCircle(GL_FILL);

	glPopMatrix();	

	glBegin(GL_POLYGON);
	glVertex2f(1, 0);
	glVertex2f(-1, 0);
	glVertex2f(-0.5, 8);
	glVertex2f(0.5, 8);
	glEnd();
}

void arm_drawSecond() {
	glPushMatrix();

	artboard_drawRoundedSquare(GL_LINE);

	glPushMatrix();

	glTranslatef(0, 8, 0);
	artboard_drawRoundedSquare(GL_LINE);

	glPopMatrix();	

	glTranslatef(0, 4, 0);
	glScalef(0.5, 8, 0);
	artboard_drawSquare(GL_FILL);

	glPopMatrix();	
}

void arm_drawThird() {
	glPushMatrix();

	glPushMatrix();

	glTranslatef(0, 4, 0);
	glScalef(0.5, 8, 0);
	artboard_drawSquare(GL_FILL);

	glPopMatrix();

	glTranslatef(0, 8, 0);
	artboard_drawCircle(GL_LINE);

	glPopMatrix();	
}

void artboard_drawAllArms(float alpha, float beta, float gamma) {
	glPushMatrix();

	glRotatef(alpha, 0, 0, 1);
	arm_drawFirst();
	glTranslatef(0, 8, 0);

	glRotatef(beta, 0, 0, 1);
	arm_drawSecond();
	glTranslatef(0, 8, 0);

	glRotatef(-gamma, 0, 0, 1);
	arm_drawThird();
	glRotatef(gamma, 0, 0, 1);
	arm_drawThird();
	glRotatef(gamma, 0, 0, 1);
	arm_drawThird();

	glPopMatrix();
}
