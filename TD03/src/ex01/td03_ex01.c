#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>

#include "colour.h"
#include "draw.h"

/* Dimensions et titre de la fenetre */
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "TD02 Minimal";

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


void reshape(SDL_Surface** surface) {
	SDL_Surface* surface_temp = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, SDL_OPENGL | SDL_RESIZABLE | SDL_GL_DOUBLEBUFFER);
	if(NULL == surface_temp) {
		fprintf(
				stderr, 
				"Erreur lors du redimensionnement de la fenetre.\n");
		return;
	}
	*surface = surface_temp;

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-4., 4., -3., 3.);
	//gluOrtho2D(-1., 1., -1., 1.);
}


int main(int argc, char** argv) 
{
	/* Initialisation de la SDL */
	if(-1 == SDL_Init(SDL_INIT_VIDEO)) 
	{
		fprintf(
				stderr, 
				"Impossible d'initialiser la SDL. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	SDL_Surface* surface;
	reshape(&surface);

	if(NULL == surface) 
	{
		fprintf(
				stderr, 
				"Impossible d'ouvrir la fenetre. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Initialisation du titre de la fenetre */
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);

	VertexPosition bluePos = {0, 0};
	VertexPosition vector = {-.08, .1};

	float alpha = 45;
	float beta = -10;
	float gamma = 35;

	int alpha_shift = 0;
	int alpha_direction = DRAW_CLOCKWISE;

	/* Boucle principale */
	int loop = 1;

	while(loop) 
	{
		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();

		/* Placer ici le code de dessin */
		glClear(GL_COLOR_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		/* Draw origin */
		artboard_drawOriginScaled();

		// Place the arm on the bottom left corner and scale it correctly
		glTranslatef(-4, -3, 0);
		glScalef(0.2,0.2,0);
		glRotatef(-90, 0, 0, 1);

		if (alpha_shift == 45) {
			alpha_direction = DRAW_CLOCKWISE;
		}
		else if (alpha_shift == -45) {
			alpha_direction = DRAW_ANTI_CLOCKWISE;
		}

		if (alpha_direction == DRAW_CLOCKWISE) {
			alpha_shift -= 1;
		}
		else {
			alpha_shift += 1;
		}

		glColor3ub(255,255,255);
		artboard_drawAllArms(alpha + alpha_shift, beta, gamma);

		/* Echange du front et du back buffer : mise a jour de la fenetre */
		SDL_GL_SwapBuffers();

		/* Boucle traitant les evenements */
		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) {

				loop = 0;
				break;
			}

			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) {

				loop = 0;
				break;
			}

			if (e.type == SDL_KEYDOWN && (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE)) {

				loop = 0; 
				break;
			}
			if (e.type == SDL_MOUSEBUTTONDOWN) {

				if (e.button.button == SDL_BUTTON_LEFT) {
					beta += 5;
				}
				else if (e.button.button == SDL_BUTTON_RIGHT) {
					beta -= 5;
				}
			}
		}

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;

		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
	}

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();

	return EXIT_SUCCESS;
}
