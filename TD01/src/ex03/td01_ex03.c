#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>

/* Dimensions de la fenetre */
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

void reshape(SDL_Surface** surface) {
	printf("reshaping...\n");
	SDL_Surface* surface_temp = SDL_SetVideoMode(
			WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, 
			SDL_OPENGL | SDL_RESIZABLE);
	if (NULL == surface_temp) {
		fprintf(stderr, "Erreur lors du redimensionnement de la fenetre.\n");
		return EXIT_FAILURE;
	}
	*surface = surface_temp;

	// TODO check reshape not working
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1., 1., -1., 1.);
}

int main(int argc, char** argv) 
{
	int draw = 0;
	/* Initialisation de la SDL */
	if(-1 == SDL_Init(SDL_INIT_VIDEO)) 
	{
		fprintf(stderr,	"Impossible d'initialiser la SDL. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	SDL_Surface* surface;
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 0);
	surface = SDL_SetVideoMode(
			WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, 
			SDL_OPENGL | SDL_RESIZABLE);
	SDL_WM_SetCaption("lololol", NULL);
	if(NULL == surface) 
	{
		fprintf(stderr, "Impossible d'ouvrir la fenetre. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	glColor3ub(128,128,128);
	/* Boucle principale */
	int loop = 1;
	glClear(GL_COLOR_BUFFER_BIT);
	while(loop) 
	{

		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();

		/* Placer ici le code de dessin */

		/* Echange du front et du back buffer : mise a jour de la fenetre */
		SDL_GL_SwapBuffers();

		/* Boucle traitant les evenements */
		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}

			/* Quelques exemples de traitement d'evenements : */
			switch(e.type) 
			{
				/* Window resize */
				case SDL_VIDEORESIZE:
					WINDOW_WIDTH = e.resize.w;
					WINDOW_HEIGHT = e.resize.h;
					reshape(&surface);
					break;

					/* Mouvement souris */
				case SDL_MOUSEMOTION:
					printf("%f, %d, %d\n", e.motion.x / surface->w * 0., e.motion.x, surface->w);
					//glClearColor((e.motion.x / surface->w), 0, (e.motion.y / surface->h), 1);
					/*
					   if (draw == 1) {
					   glBegin(GL_POINTS);
					   glVertex2f(-1 + 2. * e.motion.x / WINDOW_WIDTH, -(-1 + 2. * e.motion.y / WINDOW_HEIGHT));
					   printf("%d x %d y\n", e.motion.x, e.motion.y);
					   glEnd();
					   }
					 */
					break;

					/* Clic souris */
				case SDL_MOUSEBUTTONUP:
					//glClearColor((e.button.x % 255)/255.,(e.button.y % 255)/255., 0, 1);
					glBegin(GL_TRIANGLES);

					glVertex2f(-1 + 2. * (e.button.x) / WINDOW_WIDTH, -(-1 + 2. * (e.button.y - 20) / WINDOW_HEIGHT));
					glVertex2f(-1 + 2. * (e.button.x + 10) / WINDOW_WIDTH, -(-1 + 2. * (e.button.y + 10) / WINDOW_HEIGHT));
					glVertex2f(-1 + 2. * (e.button.x - 10) / WINDOW_WIDTH, -(-1 + 2. * (e.button.y + 10) / WINDOW_HEIGHT));
					/*
					   glVertex2f(0,1);
					   glVertex2f(-1,-1);
					   glVertex2f(1,-1);
					 */
					glEnd();
					printf("clic en (%d, %d)\n", e.button.x, e.button.y);
					draw = 0;
					break;

				case SDL_MOUSEBUTTONDOWN:
					printf("clic en (%d, %d)\n", e.button.x, e.button.y);
					draw = 1;
					/*
					   glBegin(GL_POINTS);
					   glVertex2f(-1 + 2. * e.button.x / WINDOW_WIDTH, -(-1 + 2. * e.button.y / WINDOW_HEIGHT));
					   glEnd();
					 */
					break;

					/* Touche clavier */
				case SDL_KEYDOWN:
					if (e.key.keysym.sym == 'q'){
						loop = 0;
						break;
					} 
					else if (e.key.keysym.sym == 'a') {
						glBegin(GL_QUADS);
						glVertex2f(0,0);
						glVertex2f(WINDOW_WIDTH,0);
						glVertex2f(0,WINDOW_HEIGHT);
						glVertex2f(WINDOW_WIDTH,WINDOW_HEIGHT);
						glEnd();
						glClear(GL_COLOR_BUFFER_BIT);
						break;
					}
					printf("touche pressee (code = %d)\n", e.key.keysym.sym);
					break;

				default:
					break;
			}
		}

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;
		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
	}

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();

	return EXIT_SUCCESS;
}
