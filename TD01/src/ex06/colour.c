#include "colour.h"

void colour_HSVtoRGB(VertexColour *rgb, float hue, float saturation, float value) {
	float r, g, b;
	float matchValue;

	float chroma = saturation * value;
	float hueSplit = hue / 60.;
	float absoluteValue = (fmod(hueSplit, 2)) - 1; 

	if (absoluteValue < 0) absoluteValue = -absoluteValue;

	float x = chroma * (1 - absoluteValue);		

	if (0 <= hueSplit && hueSplit < 1) {
		r = chroma;
		g = x;
		b = 0;
	} else if (1 <= hueSplit && hueSplit < 2) {
		r = x;
		g = chroma;
		b = 0;
	} else if (2 <= hueSplit && hueSplit < 3) {
		r = 0;
		g = chroma;
		b = x;
	} else if (3 <= hueSplit && hueSplit < 4) {
		r = 0;
		g = x;
		b = chroma;
	} else if (4 <= hueSplit && hueSplit < 5) {
		r = x;
		g = 0;
		b = chroma;
	} else if (5 <= hueSplit && hueSplit <= 6) {
		r = chroma;
		g = 0;
		b = x;
	}

	matchValue = value - chroma;
	rgb->r = (matchValue + r) * 255;
	rgb->g = (matchValue + g) * 255;
	rgb->b = (matchValue + b) * 255;
}
