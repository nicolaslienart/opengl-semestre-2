#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "colour.h"
#include "draw.h"

/* Dimensions de la fenetre */
static unsigned int WINDOW_WIDTH = 800;
static unsigned int WINDOW_HEIGHT = 600;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

int main(int argc, char** argv) 
{
	/* Initialisation de la SDL */
	if(SDL_Init(SDL_INIT_VIDEO) == -1) {
		fprintf(stderr,	"Impossible d'initialiser la SDL. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	SDL_Surface* surface;
	surface = SDL_SetVideoMode(
			WINDOW_WIDTH, WINDOW_HEIGHT, BIT_PER_PIXEL, 
			SDL_OPENGL | SDL_RESIZABLE | SDL_GL_DOUBLEBUFFER);
	if(NULL == surface) 
	{
		fprintf(stderr, "Impossible d'ouvrir la fenetre. Fin du programme.\n");
		return EXIT_FAILURE;
	}

	/*
	Primitive Points;
	Points.firstPoint = NULL;

	Primitive Lines;
	Lines.firstPoint = NULL;

	Primitive Triangles;
	Triangles.firstPoint = NULL;

	*/
	VertexColour currentColour = {255,255,255};

	/* Boucle principale */
	GLboolean loop = GL_TRUE;
	
	GLenum demandeMode = GL_NONE;
	GLenum currentMode = GL_POINTS;

	GLboolean redraw = GL_FALSE;
	GLboolean colourPicker = GL_FALSE;
	GLboolean freeDrawing = GL_FALSE;
	GLboolean changePossible = GL_FALSE;
	GLboolean switchMode = GL_FALSE;

	VertexPosition currentPosition;

	Primitive *primitive = NULL;

	glPointSize(4);

	while(loop) 
	{
		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();

		glClear(GL_COLOR_BUFFER_BIT);

		if (switchMode) {
			printf("Changing mode \n");
			printf("current : %d change to %d\n", currentMode, demandeMode);
			currentMode = demandeMode;
			switchMode = GL_FALSE;
			primitive = primitive_addPrimitive(primitive, currentMode);
		}


		/* Placer ici le code de dessin */
		if (colourPicker) {
			artboard_drawColorWheel(360);
		}
		else if (primitive) {
			artboard_drawAll(primitive);
		}


		/* Boucle traitant les evenements */
		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = GL_FALSE;
				break;
			}

			/* Quelques exemples de traitement d'evenements : */
			switch(e.type) 
			{
				/* Touche clavier */
				case SDL_KEYDOWN:
					switch(e.key.keysym.sym)
					{
						case 'p':
							printf("demanding to switch to points\n");
							demandeMode = GL_POINTS;
							switchMode = GL_TRUE;
							break;
						case 'l':
							printf("demanding to switch to lines\n");
							demandeMode = GL_LINES;
							switchMode = GL_TRUE;
							break;
						case 't':
							printf("demanding to switch to triangles\n");
							demandeMode = GL_TRIANGLES;
							switchMode = GL_TRUE;
							break;
						case 'm':
							colourPicker = GL_TRUE;
							break;
					}
					printf("touche pressee (code = %d)\n", e.key.keysym.sym);
					break;

				case SDL_MOUSEMOTION:
					if (freeDrawing) {
						printf("Draw at %d x, %d y\n", e.button.x, e.button.y);
						vertex_setPosition(&currentPosition, e.button.x, e.button.y, surface->w, surface->h);
						primitive_addVertex(primitive, currentPosition, currentColour);
					}
					break;

					/* Clic souris */
				case SDL_MOUSEBUTTONUP:
					freeDrawing = GL_FALSE;
					break;

				case SDL_MOUSEBUTTONDOWN:
					if (colourPicker) {
						glReadPixels(e.button.x, WINDOW_HEIGHT - e.button.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &currentColour);
						printf("current colour %d %d %d\n", currentColour.r, currentColour.g, currentColour.b);
						colourPicker = GL_FALSE;
					}
					else {
						if (!primitive) {
							primitive = primitive_addPrimitive(primitive, currentMode);
						}
						printf("Draw at %d x, %d y\n", e.button.x, e.button.y);
						vertex_setPosition(&currentPosition, e.button.x, e.button.y, surface->w, surface->h);
						primitive_addVertex(primitive, currentPosition, currentColour);
						freeDrawing = GL_TRUE;
					}
					break;
				default:
					break;
			}
		}

		SDL_GL_SwapBuffers();

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;
		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
	}

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();

	return EXIT_SUCCESS;
}
