#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 800;
static const unsigned int WINDOW_HEIGHT = 600;
static const char WINDOW_TITLE[] = "TD01 Ex06";

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


static SDL_Surface* surface = NULL;


void reshape(SDL_Surface** surface, unsigned int width, unsigned int height)
{
    SDL_Surface* surface_temp = SDL_SetVideoMode(   
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    if(NULL == surface_temp) 
    {
        fprintf(
            stderr, 
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    *surface = surface_temp;

    glViewport(0, 0, (*surface)->w, (*surface)->h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1., 1., -1., 1.);
}


/* Points */
typedef struct point_link 
{
	float x, y;               // Position 2D du point
	unsigned char r, g, b;  // Couleur du point
	struct point_link* next;    // Suivant
} Point, *PointList;

Point* allocPoint(float x, float y, unsigned char r, unsigned char g, unsigned char b)
{
	Point* new_point = (Point*) malloc(sizeof(Point));
	
	new_point->x = x;
	new_point->y = y;
	
	new_point->r = r;
	new_point->g = g;
	new_point->b = b;
	
	new_point->next = NULL;
	
	return new_point;
}

void deletePoints(PointList* list)
{
	Point* current = *list;
	Point* next;
	while (NULL != current)
	{
		next = current->next;
		free(current);
		current = next;
	}
	*list = NULL;
}

void addPoint(Point* point, PointList* list)
{
	if (NULL == *list)
	{
		*list = point;
		return;
	}

	Point *last = *list;
	while(NULL != last->next) 
	{
		last = last->next;
	}
	last->next = point;
}

void drawPoints(PointList list)
{
	while(NULL != list) 
	{
		glColor3f(list->r, list->g, list->b);
		glVertex2f(list->x, list->y);
		list = list->next;
	}
}

/* Primitives */
typedef struct primitive_link
{
	GLenum primitiveType;   // Type de primitive
	PointList points;       // Liste des vertex
	struct primitive_link* next;    // Suivant
} Primitive, *PrimitiveList;

Primitive* allocPrimitive(GLenum primitiveType)
{
	Primitive* new_primitive = (Primitive*) malloc(sizeof(Primitive));
	
	new_primitive->primitiveType = primitiveType;
	
	new_primitive->points = NULL;
	new_primitive->next = NULL;
	
	return new_primitive; 
}

void deletePrimitives(PrimitiveList* list)
{
	Primitive* current = *list;
	Primitive* next;
	while (NULL != current)
	{
		next = current->next;
		deletePoints(&(current->points));
		free(current);
		current = next;
	}
	*list = NULL;
}

void addPrimitive(Primitive* primitive, PrimitiveList* list)
{
	if (NULL == *list)
	{
		*list = primitive;
		return;
	}

	primitive->next = *list;
	*list = primitive;
}

void drawPrimitives(PrimitiveList list)
{
	while(NULL != list) 
	{
		glBegin(list->primitiveType);
		drawPoints(list->points);
		list = list->next;
		glEnd();
	}
}

/* Rectangles */
typedef struct rect_link 
{
	float x_min, y_min;   // Coin superieur gauche
	float x_max, y_max;   // Coin inferieur droit
	unsigned char r, g, b; 	// Couleur du rectangle
	struct rect_link *next; // Suivant
} Rect, *RectList;

Rect* allocRect(int x_min, int y_min, int x_max, int y_max, 
				unsigned char r, unsigned char g, unsigned char b)
{
	Rect* new_Rect = (Rect*) malloc(sizeof(Rect));
	
	new_Rect->x_min = -1 + 2. * x_min / (float) surface->w;
	new_Rect->y_min = -(-1 + 2. * y_min / (float) surface->h);
	new_Rect->x_max = -1 + 2. * x_max / (float) surface->w;
	new_Rect->y_max = -(-1 + 2. * y_max / (float) surface->h);
    
    printf("new_Rect->x_min : %f\n", new_Rect->x_min);
    printf("new_Rect->y_min : %f\n", new_Rect->y_min);
    printf("new_Rect->x_max : %f\n", new_Rect->x_max);
    printf("new_Rect->y_max : %f\n", new_Rect->y_max);

	new_Rect->r = r;
	new_Rect->g = g;
	new_Rect->b = b;

	new_Rect->next = NULL;
	
	return new_Rect;
}

void deleteRects(RectList* list)
{
	Rect* current = *list;
	Rect* next;
	while (NULL != current)
	{
		next = current->next;
		free(current);
		current = next;
	}
	*list = NULL;
}

void addRect(Rect* rect, RectList* list)
{
	if (NULL == *list)
	{
		*list = rect;
		return;
	}

	Rect *last = *list;
	while(NULL != last->next) 
	{
		last = last->next;
	}
	last->next = rect;
}

void drawRects(RectList list)
{
	while(NULL != list) 
	{
		glBegin(GL_QUADS);

		glColor3f(list->r, list->g, list->b);

		glVertex2f(list->x_min, list->y_min);
		glVertex2f(list->x_min, list->y_max);
		glVertex2f(list->x_max, list->y_max);
		glVertex2f(list->x_max, list->y_min);

		glEnd();
		list = list->next;
	}
}

int intersectsRect(Rect* Rect, int x, int y) 
{
	if(	   Rect->x_min <= (-1 + 2. * x / (float) surface->w) 
        && Rect->y_min >= -(-1 + 2. * y / (float) surface->h)
		&& Rect->x_max >= (-1 + 2. * x / (float) surface->w) 
        && Rect->y_max <= -(-1 + 2. * y / (float) surface->h))
	{
		return 1;
	}

	return 0;
}


typedef enum state 
{
	paintState = 0,
	paletteState
} State;


/* Etat actif du programme */
static State state_current = paintState;

/* Couleur de dessin */
static unsigned char r = 255, g = 0, b = 0;

/* Primitives a dessiner */
static PrimitiveList primitives = NULL;
static Primitive *primitive = NULL;

/* Description palette */
static RectList palette = NULL;


void handleEvent_paintState(SDL_Event e)
{
	switch(e.type) 
	{ 
		/* Clic souris */
			case SDL_MOUSEBUTTONUP: 
				addPoint(
                    allocPoint(
                        -1 + 2. *  e.button.x / (float) surface->w, 
					    -(-1 + 2. * e.button.y / (float) surface->h),
                        r, g, b), 
                    &(primitives->points));
				break;
		
		/* Touche clavier */
		case SDL_KEYDOWN:   
			switch (e.key.keysym.sym) 
			{
				case SDLK_p :
					primitive = allocPrimitive(GL_POINTS);
					goto finally;
				case SDLK_l :
					primitive = allocPrimitive(GL_LINES);
					goto finally;
				case SDLK_t :
					primitive = allocPrimitive(GL_TRIANGLES);
					goto finally;
				
				finally:
					addPrimitive(primitive, &primitives);
					break;
					
				default:
					break;
			}
			break;
		
		default:
			break;
  	} 
}

void handleEvent_paletteState(SDL_Event e)
{
	switch(e.type) { 
    	/* Clic souris */
		case SDL_MOUSEBUTTONUP:
		{
			Rect* current = palette;
			while (NULL != current)
			{
				if(intersectsRect(current, e.button.x, e.button.y))
				{
					printf("clic sur Rect palette, r=%i , g=%i, b=%i\n", current->r, current->g, current->b);

					r = current->r;
					g = current->g;
					b = current->b;
					break;
				}
				current = current->next;
			}
			break;
		}
				
    	default:
      		break;
  	} 
}

void draw_paintState()
{
	drawPrimitives(primitives);
}

void draw_paletteState()
{
	drawRects(palette);
}


int main(int argc, char** argv) 
{
  	/* Initialisation de la SDL */
  	if(-1 == SDL_Init(SDL_INIT_VIDEO)) 
	{
    	fprintf(
            stderr, 
            "Impossible d'initialiser la SDL. Fin du programme.\n");
    	return EXIT_FAILURE;
  	}
  
	/* Ouverture d'une fenetre et creation d'un contexte OpenGL */
	reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1., 1., -1., 1.);
  
	/* Initialisation du titre de la fenetre */
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);
  
    /* Initialisation de l'apparence des points */
  	glPointSize(4);
  
	/* Initalisation primitives et palette */
  	addPrimitive(allocPrimitive(GL_POINTS), &primitives);
  
	addRect(allocRect(50, 50, 150, 150, 255, 0, 0), &palette);
	addRect(allocRect(200, 50, 300, 150, 0, 255, 0), &palette);
	addRect(allocRect(350, 50, 450, 150, 0, 0, 255), &palette);
	addRect(allocRect(500, 50, 600, 150, 255, 255, 255), &palette);


  	/* Boucle principale */
  	int loop = 1;
  	while(loop) 
	{
		/* Recuperation du temps au debut de la boucle */
		Uint32 startTime = SDL_GetTicks();
		
		/* Placer ici le code de dessin */
		glClear(GL_COLOR_BUFFER_BIT);
		
		/* Affichage specifique a chaque etat */
		switch(state_current) 
		{
			case paintState:
				draw_paintState();
				break;
			case paletteState:
				draw_paletteState();
				break;
			
			default:
				break;
		}
		
		/* Echange du front et du back buffer : mise a jour de la fenetre */
		SDL_GL_SwapBuffers();
		
		/* Boucle traitant les evenements */
		SDL_Event e;
		while(SDL_PollEvent(&e)) 
		{
			/* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}
		
			if(	e.type == SDL_KEYDOWN 
				&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
			{
				loop = 0; 
				break;
			}
		
			/* Quelques exemples de traitement d'evenements : */
			switch(e.type) 
			{
				/* Redimensionnement fenetre */
				case SDL_VIDEORESIZE:
					reshape(&surface, e.resize.w, e.resize.h);
					break; 
			
				/* Clic souris */
				case SDL_MOUSEBUTTONUP:
					printf("clic en (%d, %d)\n", e.button.x, e.button.y);
					break;
				
				/* Touche clavier */
				case SDL_KEYDOWN:
					printf("touche pressee (code = %d)\n", e.key.keysym.sym);        
				
					/* Passage en mode palette */
					if(e.key.keysym.sym == SDLK_SPACE)
					{
						state_current = paletteState;
					}
					break;

				case SDL_KEYUP:
					printf("touche relachee (code = %d)\n", e.key.keysym.sym);        
				
					/* Passage en mode dessin */
					if(e.key.keysym.sym == SDLK_SPACE)
					{
						state_current = paintState;
					}
					break;
				
				default:
				break;
			}
			
			/* Gestion des evenements specifiques a chaque etat */
			switch(state_current) 
			{
				case paintState:              
					handleEvent_paintState(e);
					break;
				case paletteState:
					handleEvent_paletteState(e);
					break;
				
				default:
					break;
			}
		}

		/* Calcul du temps ecoule */
		Uint32 elapsedTime = SDL_GetTicks() - startTime;
		/* Si trop peu de temps s'est ecoule, on met en pause le programme */
		if(elapsedTime < FRAMERATE_MILLISECONDS) 
		{
			SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
		}
  	}
  
    /* Liberation de la memoire allouee */
	deleteRects(&palette);
	deletePrimitives(&primitives);

	/* Liberation des ressources associees a la SDL */ 
	SDL_Quit();
	
	return EXIT_SUCCESS;
}
